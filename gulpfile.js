const { src, dest, watch } = require('gulp')
const sass = require('gulp-sass')

function compile_sass () {
  return src('./scss/style.scss')
    .pipe(sass())
    .pipe(dest('./styles'))
}

exports.default = function () {
  watch('./scss/style.scss', compile_sass)
}
