'use strict'
AOS.init({
    easing: 'ease-out',
    duration: 1000
});

/* animation des fleches du menu */
// animation cause des erreurs ????????????????

// const menu = document.getElementById('liste-menu')
// const ligneVersFleche = document.getElementsByClassName('ligne-vers-fleche')
// const flecheVersLigne = document.getElementsByClassName('fleche-vers-ligne')

// let index

// menu.addEventListener('mouseover', (e) => {
//     index = e.target.name
//     ligneVersFleche[parseInt(index)].beginElement('to')
// })

// menu.addEventListener('mouseout', () => {
//     flecheVersLigne[parseInt(index)].beginElement()
// })

/* animation maison menu */
const maisonAcceuil = document.getElementById('maison-acceuil')
const maison = document.getElementById('maison')


maisonAcceuil.addEventListener('click', (e) => {
    e.preventDefault()
    maison.style.visibility = "visible"
    maisonAcceuil.classList.remove("maison-sen-vient")
    maisonAcceuil.classList.add("maison-sen-va")
    maison.classList.add("maison-fait-tour")
    setTimeout(() => {
    maison.style.visibility = "hidden"
    maisonAcceuil.classList.remove("maison-sen-va")
    maisonAcceuil.classList.add("maison-sen-vient")
    maison.classList.remove("maison-fait-tour")
    }, 15600)
})

// Animation pour modal
const ouvrirModal = document.getElementById('ouvrir-modal')
const modal = document.getElementById('modal')
const bouton = document.getElementById('fermer')
const contenuModal = document.getElementsByClassName('modal-content')
const headerModal = document.getElementsByClassName('modal-header')

ouvrirModal.onclick = () => {
    modal.style.display = "block"
    setTimeout(() => {
        contenuModal[0].classList.add("animationModal")
        headerModal[0].classList.add("animationModal")
    }, 100)
}

bouton.onclick = () => {
    modal.style.display = "none"
    contenuModal[0].classList.remove("animationModal")
    headerModal[0].classList.remove("animationModal")
}