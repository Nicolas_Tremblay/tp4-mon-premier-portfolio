*Toutes les animations sont sur la page d'acceuil et le css dans le document _animation.scss*

**1. Trois animations par transition**
- Sur le menu, une animation est mise sur chaque élément qui s'active avec un hover.
- Lorsque l'on scroll vers le bas les photos en parallaxe et les titres en noir apparaissent.
- Dans le milieu de la page d'accueil un rectangle gris avec le texte défiler vers le bas et entourez de deux animations svg.

**2. Une animation par Keyframes**
- Sur la page d'accueil seulement, cliquer sur la maison active une animation qui fait le tour de l'écran à trois reprises.

**3. Une animation contrôlée par JavaScript**
- Dans le footer de la page d'accueil, cliquer sur la phrase "joignez notre infolettre" fait apparaître un formulaire par-dessus le contenu du site.
